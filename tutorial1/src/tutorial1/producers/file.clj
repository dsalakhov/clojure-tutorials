(ns tutorial1.producers.file
  (:require [clojure.java.io :as io]))

(defn read-quips
  "Returns vector of quips or nil if file doesn't exist"
  [file]
  (when (.exists (io/file file))
    (with-open [rdr (io/reader file)]
      (mapv read-string (line-seq rdr)))))

(defn write-quips
  "Writes collection of quips to the file"
  [file quips]
  (with-open [wrt (io/writer file :append true)]
    (doseq [line quips]
      (.write wrt (prn-str line)))))

(defn drop-quips [file]
  (io/delete-file file))

(defn count-quips
  [file]
  (count (read-quips file)))