(ns tutorial1.producers.quipper-responder
  (:require [tutorial1.producers.file :as storage]
            [ring.util.response :as r]
            [schema.core :as s]))

(s/defn add-quip
  [file :- s/Str quips]
  (storage/write-quips file quips)
  (-> (r/response {:quips (storage/read-quips file)})
      (r/status 201)))

(s/defn clean-up
  [file :- s/Str]
  (storage/drop-quips file)
  {:status 204})

(s/defn get-random-quip
  [file :- s/Str]
  (-> (if-let [quips (seq (storage/read-quips file))]
        (rand-nth quips)
        {})
      (r/response)))

(s/defn get-count
  [file :- s/Str]
  (-> {:count (storage/count-quips file)}
      (r/response)))
